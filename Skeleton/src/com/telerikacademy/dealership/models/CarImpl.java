package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;

public class CarImpl extends VehicleBase implements Car {
    private static final int MIN_SEATS = 1;
    private static final int MAX_SEATS = 10;
    private static final String ERROR_SEATS_MESSAGE = "Seats must be between %d and %d!";
    private static final VehicleType type = VehicleType.CAR;
    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, type);
        setSeats(seats);
    }


    protected String getSeatsIllegalArgumentMessage() {
        return String.format(ERROR_SEATS_MESSAGE,getMinSeatsValue(),getMaxSeatsValue());
    }

    private void setSeats(int seats) {
        Validator.ValidateIntRange(seats,MIN_SEATS,MAX_SEATS,getSeatsIllegalArgumentMessage());
        this.seats = seats;
    }
    protected int getMinSeatsValue()
    {
        return MIN_SEATS;
    }
    protected int getMaxSeatsValue()
    {
        return MAX_SEATS;
    }

    @Override
    public int getSeats() {
        return seats;
    }

    @Override
    public int getWheels() {
        return type.getWheelsFromType();
    }

    @Override
    public VehicleType getType() {
        return type;
    }
    @Override
    protected String printAdditionalInfo() {
        return String.format(" Seats: %d",getSeats());
    }

    
    
}
