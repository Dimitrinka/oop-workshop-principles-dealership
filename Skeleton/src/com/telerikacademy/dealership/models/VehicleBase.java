package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {

    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";
    private final static String ERROR_MESSAGE = "%s must be between %d and %d characters long!";
    private final static String NULL_ERROR_MESSAGE = "Cannot be null";
    private final static String ERROR_PRICE = "Price must be between %.1f and %.1f!";
    private final static int MIN_MAKE_LENGTH = 2;
    private final static int MAX_MAKE_LENGTH = 15;
    private final static int MIN_MODEL_LENGTH = 1;
    private final static int MAX_MODEL_LENGTH = 15;
    private final static double MIN_PRICE = 0.0;
    private final static double MAX_PRICE = 1000000.0;

    protected List<Comment> comments;
    private String make;
    private String model;
    private double price;
    private VehicleType type;

    VehicleBase(String make, String model, double price, VehicleType vehicleType) {
        setMake(make);
        setModel(model);
        setPrice(price);
        this.type = vehicleType;
        comments = new ArrayList<>();
    }
    @Override
    public String getMake() {
        return make;
    }

    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }
    @Override
    public void removeComment(Comment comment) {
        Validator.ValidateNull(comment,ModelsConstants.COMMENT_CANNOT_BE_NULL);

        comments.remove(comment);
    }

    @Override
    public void addComment(Comment comment) {
        Validator.ValidateNull(comment,ModelsConstants.COMMENT_CANNOT_BE_NULL);
        comments.add(comment);
    }


    private void setMake(String make) {
        Validator.ValidateNull(make,NULL_ERROR_MESSAGE);
        Validator.ValidateIntRange(make.length(),MIN_MAKE_LENGTH,MAX_MAKE_LENGTH,getMakeIllegalArgumentMessage());
        this.make = make;
    }
    protected String getMakeIllegalArgumentMessage() {
        return String.format(ERROR_MESSAGE,MAKE_FIELD, getMinValueMakeLength(), getMaxValueMakeLength());
    }
    protected int getMinValueMakeLength() {
        return MIN_MAKE_LENGTH;
    }

    protected int getMaxValueMakeLength() {
        return MAX_MAKE_LENGTH;
    }

    @Override
    public String getModel() {
        return model;
    }

    private void setModel(String model) {
        Validator.ValidateNull(model,NULL_ERROR_MESSAGE);
        Validator.ValidateIntRange(model.length(),MIN_MODEL_LENGTH,MAX_MODEL_LENGTH,getModelIllegalArgumentMessage());
        this.model = model;
    }
    protected String getModelIllegalArgumentMessage() {
        return String.format(ERROR_MESSAGE,MODEL_FIELD, getMinValueModelLength(), getMaxValueModelLength());
    }
    protected int getMinValueModelLength() {
        return MIN_MODEL_LENGTH;
    }

    protected int getMaxValueModelLength() {
        return MAX_MODEL_LENGTH;
    }


    @Override
    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        Validator.ValidateDecimalRange(price,MIN_PRICE,MAX_PRICE,getPriceIllegalArgumentMessage());
        this.price = price;
    }
    private double getMinValuePrice()
    {
        return MIN_PRICE;
    }
    private double getMaxValuePrice()
    {
        return MAX_PRICE;
    }
    protected String getPriceIllegalArgumentMessage() {
        return String.format(ERROR_PRICE,getMinValuePrice(), getMaxValuePrice());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        //finish implementation of toString() - what should the next 3 lines of code append to the builder?
        builder.append(String.format("  %s: %s",MAKE_FIELD,getMake())).append(System.lineSeparator());
        builder.append(String.format("  %s: %s",MODEL_FIELD,getModel())).append(System.lineSeparator());
        builder.append(String.format("  %s: %d",WHEELS_FIELD,type.getWheelsFromType())).append(System.lineSeparator());

        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(getPrice()))).append(System.lineSeparator());
        
        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }

    //todo replace this comment with explanation why this method is protected:
    //This method is protected, because I want to access it from the classes in the same package or from its subclasses.
    protected abstract String printAdditionalInfo();
    
    private String printComments() {
        StringBuilder builder = new StringBuilder();
        
        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());
            
            for (Comment comment : comments) {
                builder.append(comment.toString());
            }
            
            builder.append(String.format("%s", COMMENTS_HEADER));
        }
        
        return builder.toString();
    }
    
}
