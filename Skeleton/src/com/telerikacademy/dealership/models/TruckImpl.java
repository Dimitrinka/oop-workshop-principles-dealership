package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.Role;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Truck;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class TruckImpl extends VehicleBase implements Truck {
    private static final VehicleType type = VehicleType.TRUCK;
    private static final int MIN_WEIGHT_CAPACITY = 1;
    private static final int MAX_WEIGHT_CAPACITY = 100;
    private static final String ERROR_WEIGHT_CAPACITY = "Weight capacity must be between %d and %d!";
    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, type);
        setWeightCapacity(weightCapacity);
    }

    private void setWeightCapacity(int weightCapacity) {
        Validator.ValidateIntRange(weightCapacity,MIN_WEIGHT_CAPACITY,MAX_WEIGHT_CAPACITY,getWeightCapacityIllegalArgumentMessage());
        this.weightCapacity = weightCapacity;
    }
    private String getWeightCapacityIllegalArgumentMessage() {
        return String.format(ERROR_WEIGHT_CAPACITY, getMinWeightCapacity(), getMaxWeightCapacity());
    }
    private int getMinWeightCapacity()
    {
        return MIN_WEIGHT_CAPACITY;
    }
    private int getMaxWeightCapacity()
    {
        return MAX_WEIGHT_CAPACITY;
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }
    @Override
    protected String printAdditionalInfo() {
        return String.format(" Weight Capacity: %dt",getWeightCapacity());
    }

    @Override
    public int getWheels() {
        return type.getWheelsFromType();
    }

    @Override
    public VehicleType getType() {
        return type;
    }




}
