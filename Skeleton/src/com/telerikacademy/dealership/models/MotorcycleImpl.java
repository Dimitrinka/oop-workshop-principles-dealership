package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;

import com.telerikacademy.dealership.models.contracts.Motorcycle;


public class MotorcycleImpl extends VehicleBase implements Motorcycle  {
    private static final VehicleType type = VehicleType.MOTORCYCLE;
    private static final int MIN_CATEGORY_LENGTH = 3;
    private static final int MAX_CATEGORY_LENGTH = 10;
    private static final String ERROR_CATEGORY_LENGTH_MESSAGE = "Category must be between %d and %d characters long!";
    private String category;


    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, type);
        setCategory(category);
    }

    private void setCategory(String category) {
       // Category must be between 3 and 10 characters long!
        Validator.ValidateIntRange(category.length(),MIN_CATEGORY_LENGTH,MAX_CATEGORY_LENGTH,getErrorCategoryLengthMessage());
        this.category = category;
    }
    protected String getErrorCategoryLengthMessage() {
        return String.format(ERROR_CATEGORY_LENGTH_MESSAGE, MIN_CATEGORY_LENGTH, MAX_CATEGORY_LENGTH);
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public int getWheels() {
        return VehicleType.MOTORCYCLE.getWheelsFromType();
    }

    @Override
    public VehicleType getType() {
        return type;
    }





    @Override
    protected String printAdditionalInfo() {
        return String.format(" Category: %s",getCategory());
    }


    //look in DealershipFactoryImpl - use it to create proper constructor
}
